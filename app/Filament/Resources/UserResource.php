<?php

namespace App\Filament\Resources;

use App\Filament\Resources\UserResource\Pages;
use App\Models\User;
use Filament\Forms\Components\Checkbox;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Illuminate\Support\Facades\Hash;

class UserResource extends Resource
{
    protected static ?string $model = User::class;

    protected static ?string $navigationIcon = 'heroicon-o-user';

    protected static ?string $navigationGroup = 'User Management';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make()
                    ->schema([
                        TextInput::make('name')
                            ->maxLength(255)
                            ->required(),
                        TextInput::make('email')
                            ->email()
                            ->maxLength(255)
                            ->required(),
                        TextInput::make('password')
                            ->password()
                            ->revealable() // отображаем пароль, нажав на глазик
                            ->same('passwordConfirmation') // нужно подтвердить пароль, пароль должен совпадать с паролем подтверждения
                            ->dehydrateStateUsing(fn (string $state): string => Hash::make($state)) // при submit формы хешировать пароль
                            ->dehydrated(fn (?string $state): bool => filled($state)) // не перезаписывать пароль, если он незаполнен
                            ->minLength(8)
                            ->required(fn (string $operation): bool => $operation === 'create'),
                        TextInput::make('passwordConfirmation')
                            ->password()
                            ->minLength(8)
                            ->dehydrated(false)
                            ->required(fn (string $operation): bool => $operation === 'create'), // обязательно для create
                        Checkbox::make('remember_token')->columnStart(1),
                        TextInput::make('email_verified_at')->columnStart(1)
                            ->readOnly(),
                    ])->columns(2),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('id')->sortable()->searchable()->label('ID'),
                TextColumn::make('name')->sortable()->searchable()->label('Имя'),
                TextColumn::make('email')->sortable()->searchable()->label('Email'),
                TextColumn::make('created_at')->dateTime('Y-m-d H:i:s'),
                TextColumn::make('updated_at')->dateTime('Y-m-d H:i:s'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListUsers::route('/'),
            'create' => Pages\CreateUser::route('/create'),
            'edit' => Pages\EditUser::route('/{record}/edit'),
        ];
    }
}
