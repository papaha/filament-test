up: clear down startall
down: docker-stop

all: clean test

test:
	vendor/bin/phpunit

coverage:
	vendor/bin/phpunit --coverage-html=artifacts/coverage

view-coverage:
	open artifacts/coverage/index.html

clean:
	rm -rf artifacts/*

clear:
	@clear
docker-stop:
	@echo ----------- Останавливаем все контейнеры -----------------
	docker ps -q --filter "status=running" | xargs -r docker stop

startall:
	@echo ----------- Поднимаем laravel-filament -----------------
	./vendor/bin/sail up -d
	@echo ----------- Готово -----------------

build:
	@clear
	@echo ----------- Build -----------------
	./vendor/bin/sail up build
	@echo ----------- Готово -----------------
